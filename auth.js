module.exports = {
    ensureAuthenticated: function (req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        req.flash('error_msg', 'Kirjaudu sisään ensin');
        res.redirect('/');
    },
    forwardAuthenticated: function (req, res, next) {
        if (!req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    },
    //Check if the current user has admin access
    isAdmin(req, res, next) {
        if (req.user.isAdmin === false) {
            res.redirect('/dashboard')
        } else {
            next();
        }
    },

};